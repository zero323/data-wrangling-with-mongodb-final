
#### Name:
Maciej Szymkiewicz

#### E-mail:
zero323@users.noreply.github.com

#### URL:
https://www.openstreetmap.org/export#map=12/52.2238/20.9900

#### Reason:
- Warsaw is my hometown so I think it is pretty obvious choice.
- I know this area well enough to spot important errors and I have some
knowledge about additional sources which can be used to improve data quality.

#### Notes:
- lxml.etree module has been used instead of standard library xml.etree. I have
posted some explanation [on the course
forum](http://forums.udacity.com/questions/100159785/audit-function-is-clearly-
wrong#ud032)
- example.osm URL: https://www.openstreetmap.org/export#map=16/52.2403/20.9746



    import numpy as np
    import os
    import pymongo
    import pandas
    import re
    import requests
    from functools import partial
    from fuzzywuzzy import process
    
    from tools import stats, cleanup, datasets, teryt
    import data
    
    GOOGLE_GEOCODING_API_KEY = os.environ.get('GOOGLE_GEOCODING_API_KEY')
    DATABASE = 'udacity'
    
    LONG_MIN = 20.8672
    LAT_MIN = 52.1322
    LONG_MAX = 21.1202
    LAT_MAX = 52.3154

First we should remove existing data


    con = pymongo.MongoClient()
    con.drop_database(DATABASE)
    db = con[DATABASE] 

Download OpenStreetMap data


    osd_input_file, osd_data_size = datasets.get_osd_data(LONG_MIN, LAT_MIN, LONG_MAX, LAT_MAX)  # Imort OSM data
    print "Dataset size: {0:.0f}MB".format(osd_data_size)

    Dataset size: 142MB


Import OpenStreetMap into MongoDB


    _ = db.raw.insert(data.process_map(osd_input_file))
    db.raw.create_index([('address.city', 1)])
    db.raw.create_index([('address.street', 1)])
    print 'Number of imported elements: {0}'.format(db.raw.find().count())

    Number of imported elements: 690156


## Basic statistics

### Users


    users = stats.count_users(db, 'raw')
    users.describe().transpose()




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>25%</th>
      <th>50%</th>
      <th>75%</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td> 1019</td>
      <td> 677.287537</td>
      <td> 5700.814736</td>
      <td> 1</td>
      <td> 2</td>
      <td> 8</td>
      <td> 54</td>
      <td> 131326</td>
    </tr>
  </tbody>
</table>
<p>1 rows × 8 columns</p>
</div>




    _ = users['count'].hist()


![png](summary_files/summary_11_0.png)


1019 individual users contributed to given dataset. Distibution of number of
submitions is highly positively skewed with mean 676.54367 and median 8.

### Types


    stats.count_types(db, 'raw')




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>_id</th>
      <th>count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0 </th>
      <td>         node</td>
      <td> 592821</td>
    </tr>
    <tr>
      <th>1 </th>
      <td>          way</td>
      <td>  97279</td>
    </tr>
    <tr>
      <th>2 </th>
      <td>         Lipa</td>
      <td>     17</td>
    </tr>
    <tr>
      <th>3 </th>
      <td> broad_leafed</td>
      <td>     12</td>
    </tr>
    <tr>
      <th>4 </th>
      <td>        route</td>
      <td>      8</td>
    </tr>
    <tr>
      <th>5 </th>
      <td>       plaque</td>
      <td>      5</td>
    </tr>
    <tr>
      <th>6 </th>
      <td> multipolygon</td>
      <td>      4</td>
    </tr>
    <tr>
      <th>7 </th>
      <td>         heat</td>
      <td>      3</td>
    </tr>
    <tr>
      <th>8 </th>
      <td> broad_leaved</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>9 </th>
      <td> construction</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>10</th>
      <td>       public</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>11</th>
      <td>         palm</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>12</th>
      <td>       shrine</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>13</th>
      <td>        audio</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>14</th>
      <td>    lift_gate</td>
      <td>      1</td>
    </tr>
  </tbody>
</table>
<p>15 rows × 2 columns</p>
</div>



Since 'type' can be used to describe certain types of the nodes it is rather
obvious that using 'type' field to store tag names wasn't the best idea. Since
number of the affected nodes is rather small I will ignore this issue right now
but generally speaking it would be better to use [OSM JSON](http://overpass-
api.de/output_formats.html#json) to represent our data.

Even if we ignore 'type' ambiguity we can see that some entries are probably
wrong and can be easily fixed. For example "Lipa" is a Polish name for
[Tilia](https://en.wikipedia.org/wiki/Tilia) so my guess is that it is a simple
tagging error.



    db.raw.find_one({'type': 'Lipa'})




    {u'_id': ObjectId('53464e700325b1068ecfdd4c'),
     u'created': {u'changeset': u'13234680',
      u'timestamp': u'2012-09-24T14:53:02Z',
      u'uid': u'99386',
      u'user': u'masti',
      u'version': u'1'},
     u'id': u'1930590233',
     u'natural': u'tree',
     u'pos': [52.1893696, 20.9807286],
     u'type': u'Lipa'}




    set(x['created']['user'] for x in db.raw.find({'type': 'Lipa'}))  # Check how many users tagged nodes as "Lipa"




    {u'masti'}



Since only one user used `"type: Lipa"` we can assume all of these follow the
same pattern and be easily fixed to follow [OpenStreetMap
guidelines](http://wiki.openstreetmap.org/wiki/Tag:natural%3Dtree)


    db.raw.update({'type': 'Lipa'}, {'$set': {'type': 'broad_leaved', 'genus': 'Tilia'}}, multi=True)




    {u'connectionId': 22,
     u'err': None,
     u'n': 17,
     u'ok': 1.0,
     u'updatedExisting': True}



Similarly we can replace `"broad_leafed"` type with `"broad_leaved"`.


    db.raw.update({'type': 'broad_leafed'}, {'$set': {'type': 'broad_leaved'}}, multi=True)




    {u'connectionId': 22,
     u'err': None,
     u'n': 12,
     u'ok': 1.0,
     u'updatedExisting': True}




    assert db.raw.find({'type': 'broad_leaved'}).count() == 30 and db.raw.find({'type': 'Lipa'}).count() == 0

Another type which looks suspicious is `"lift_gate"`.


    lift_gate = db.raw.find_one({'type': 'lift_gate'})
    lift_gate 




    {u'_id': ObjectId('53464e6d0325b1068ecb4c47'),
     u'barrier': u'gate',
     u'created': {u'changeset': u'7210605',
      u'timestamp': u'2011-02-06T22:03:09Z',
      u'uid': u'165509',
      u'user': u'koszatek',
      u'version': u'4'},
     u'id': u'275037714',
     u'pos': [52.2507167, 21.1097245],
     u'type': u'lift_gate'}




    db.raw.find({'barrier': 'lift_gate'}).distinct('type')




    [u'node']



Since lift gates should be represented as nodes [(wiki.openstreetmap.org:Lift_ga
te)](http://wiki.openstreetmap.org/wiki/Lift_gate) and almost all of the entries
in our dataset are formed correctly we should probably fix this one.


    lift_gate [u'type'] = u'node'
    lift_gate [u'barier'] = u'lift_gate'
    db.raw.save(lift_gate)




    ObjectId('53464e6d0325b1068ecb4c47')




    assert db.raw.find({'type': 'lift_gate'}).count() == 0

## Quality check

### Geospatial coverage

Let's see if imported dataset covers expected area.


    all_query = {'pos': {'$exists': 1}}
    df = pandas.DataFrame({'lat': x['pos'][0], 'long': x['pos'][1]} for x in db.raw.find(all_query))
    
    pandas.DataFrame(
        [
            ('Latitude min', LAT_MIN, df.lat.min(), LAT_MIN <= df.lat.min()),
            ('Latitude max', LAT_MAX, df.lat.max(), LAT_MAX >= df.lat.max()),
            ('Longitude min', LONG_MIN, df.long.min(), LONG_MIN <= df.long.min()),
            ('Longitude max', LONG_MAX, df.long.max(), LONG_MAX >= df.long.max())
        ],
        columns = ['', 'Expected', 'Actual', 'Correct']
    )




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>Expected</th>
      <th>Actual</th>
      <th>Correct</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>  Latitude min</td>
      <td> 52.1322</td>
      <td> 51.810386</td>
      <td> False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>  Latitude max</td>
      <td> 52.3154</td>
      <td> 52.426673</td>
      <td> False</td>
    </tr>
    <tr>
      <th>2</th>
      <td> Longitude min</td>
      <td> 20.8672</td>
      <td> 19.625581</td>
      <td> False</td>
    </tr>
    <tr>
      <th>3</th>
      <td> Longitude max</td>
      <td> 21.1202</td>
      <td> 21.250305</td>
      <td> False</td>
    </tr>
  </tbody>
</table>
<p>4 rows × 4 columns</p>
</div>



Clearly something is not right here. We can polt distribution of the nodes to
see what is going on.


    stats.plot_histogram2d(stats.get_histogram2d(df), log=True)


![png](summary_files/summary_34_0.png)


It seems like our dataset contains not only the nodes from the selected area but
also some exit routes and some railway stations from the surrounding area.


    # Select only the nodes from the area of interest
    inside_query = {
        'pos': {'$exists': 1},
        'pos.0': {'$gte': LAT_MIN, '$lte': LAT_MAX},
        'pos.1': {'$gte': LONG_MIN, '$lte': LONG_MAX},
    }
    
    print 'Number of nodes outside the area of interest: {0}'.format(db.raw.find(all_query).count() - db.raw.find(inside_query).count())

    Number of nodes outside the area of interest: 8235


Since number of the problematic nodes is relatively small we can ignore it for a
time being but should keep this in mind and address properly if needed. Right
now let's focus on the actual area of interest.


    df = pandas.DataFrame({'lat': x['pos'][0], 'long': x['pos'][1]} for x in db.raw.find(inside_query))
    stats.plot_histogram2d(stats.get_histogram2d(df, 20), log=True)


![png](summary_files/summary_38_0.png)


Everything looks more or less as expected even if compared to the [view of
Warsaw from the International Space Station](https://upload.wikimedia.org/wikipe
dia/commons/b/bb/WarsawFromTheISS.jpg).

## Cities


    stats.count_cities(db, 'raw')




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>_id</th>
      <th>count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0 </th>
      <td>                          None</td>
      <td> 633167</td>
    </tr>
    <tr>
      <th>1 </th>
      <td>                      Warszawa</td>
      <td>  48166</td>
    </tr>
    <tr>
      <th>2 </th>
      <td>                        Raszyn</td>
      <td>   2010</td>
    </tr>
    <tr>
      <th>3 </th>
      <td>                         Rybie</td>
      <td>   1521</td>
    </tr>
    <tr>
      <th>4 </th>
      <td>                   Michałowice</td>
      <td>   1155</td>
    </tr>
    <tr>
      <th>5 </th>
      <td>                 Opacz-Kolonia</td>
      <td>    630</td>
    </tr>
    <tr>
      <th>6 </th>
      <td>               Nowe Grocholice</td>
      <td>    395</td>
    </tr>
    <tr>
      <th>7 </th>
      <td>            Blizne Jasińskiego</td>
      <td>    343</td>
    </tr>
    <tr>
      <th>8 </th>
      <td>                      Jaworowa</td>
      <td>    289</td>
    </tr>
    <tr>
      <th>9 </th>
      <td>                         Ząbki</td>
      <td>    254</td>
    </tr>
    <tr>
      <th>10</th>
      <td>         Blizne Łaszczyńskiego</td>
      <td>    241</td>
    </tr>
    <tr>
      <th>11</th>
      <td>       Warszawa@Warszawa Praga</td>
      <td>    206</td>
    </tr>
    <tr>
      <th>12</th>
      <td>              Michałowice-Wieś</td>
      <td>    153</td>
    </tr>
    <tr>
      <th>13</th>
      <td>                         Janki</td>
      <td>    146</td>
    </tr>
    <tr>
      <th>14</th>
      <td>     Warszawa@Warszawa Mokotów</td>
      <td>    142</td>
    </tr>
    <tr>
      <th>15</th>
      <td>                  Falenty Nowe</td>
      <td>    139</td>
    </tr>
    <tr>
      <th>16</th>
      <td>                       Falenty</td>
      <td>    123</td>
    </tr>
    <tr>
      <th>17</th>
      <td>                        Dawidy</td>
      <td>    105</td>
    </tr>
    <tr>
      <th>18</th>
      <td>                      Mościska</td>
      <td>    100</td>
    </tr>
    <tr>
      <th>19</th>
      <td>                       Puchały</td>
      <td>     96</td>
    </tr>
    <tr>
      <th>20</th>
      <td>     Warszawa@Warszawa Ursynów</td>
      <td>     95</td>
    </tr>
    <tr>
      <th>21</th>
      <td>                    Opacz Mała</td>
      <td>     89</td>
    </tr>
    <tr>
      <th>22</th>
      <td>                        Reguły</td>
      <td>     80</td>
    </tr>
    <tr>
      <th>23</th>
      <td>                       Sokołów</td>
      <td>     73</td>
    </tr>
    <tr>
      <th>24</th>
      <td>     Warszawa@Warszawa Bielany</td>
      <td>     55</td>
    </tr>
    <tr>
      <th>25</th>
      <td>                        Wypędy</td>
      <td>     51</td>
    </tr>
    <tr>
      <th>26</th>
      <td>                Dawidy Bankowe</td>
      <td>     49</td>
    </tr>
    <tr>
      <th>27</th>
      <td>                       Szeligi</td>
      <td>     48</td>
    </tr>
    <tr>
      <th>28</th>
      <td>                        Warsaw</td>
      <td>     43</td>
    </tr>
    <tr>
      <th>29</th>
      <td>      Warszawa@Warszawa Ochota</td>
      <td>     41</td>
    </tr>
    <tr>
      <th>30</th>
      <td>                  Falenty Duże</td>
      <td>     26</td>
    </tr>
    <tr>
      <th>31</th>
      <td>                          Mory</td>
      <td>     26</td>
    </tr>
    <tr>
      <th>32</th>
      <td>                      Lubiczów</td>
      <td>     23</td>
    </tr>
    <tr>
      <th>33</th>
      <td> Warszawa@Warszawa Śródmieście</td>
      <td>     14</td>
    </tr>
    <tr>
      <th>34</th>
      <td>                         Marki</td>
      <td>     13</td>
    </tr>
    <tr>
      <th>35</th>
      <td>                    Macierzysz</td>
      <td>     11</td>
    </tr>
    <tr>
      <th>36</th>
      <td>       Warszawa@Warszawa Ursus</td>
      <td>      9</td>
    </tr>
    <tr>
      <th>37</th>
      <td>                      warszawa</td>
      <td>      5</td>
    </tr>
    <tr>
      <th>38</th>
      <td>        Warszawa@Warszawa Wola</td>
      <td>      4</td>
    </tr>
    <tr>
      <th>39</th>
      <td>                       Klaudyn</td>
      <td>      3</td>
    </tr>
    <tr>
      <th>40</th>
      <td>                        Płocka</td>
      <td>      3</td>
    </tr>
    <tr>
      <th>41</th>
      <td>      Warszawa@Warszawa Włochy</td>
      <td>      3</td>
    </tr>
    <tr>
      <th>42</th>
      <td>                        warsaw</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>43</th>
      <td>                            32</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>44</th>
      <td>      Warszawa@Warszawa Bemowo</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>45</th>
      <td>     Warszawa@Warszawa Wilanów</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>46</th>
      <td>                    Latchorzew</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>47</th>
      <td>                             2</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>48</th>
      <td>                            34</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>49</th>
      <td>                         Laski</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>50</th>
      <td>             Warszawa, Ludwiki</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>51</th>
      <td>                        Błonie</td>
      <td>      1</td>
    </tr>
    <tr>
      <th>52</th>
      <td>                           139</td>
      <td>      1</td>
    </tr>
  </tbody>
</table>
<p>53 rows × 2 columns</p>
</div>



It is rather obvious that some of the above are either malformed or simply
wrong. It would be easy to identify some of the malformed entries without any
additional data but we can do it as well in a smarter way using [National
Official Register of Territorial Division of the Country
(TERYT)](http://www.stat.gov.pl/bip/36_ENG_HTML.htm)

> TERYT register contains systems of:
>
> - identifiers and names of units of territorial division,
> - identifiers and names of localities,
> - statistical regions and census enumeration areas,
> - identification of addresses of streets, real estates, buildings and
dwellings.


    # Import TERYT data
    _ = db.terc.insert(teryt.parse(datasets.get_teryt_data('TERC'))) # System of identifiers and names of territorial division units TERC
    _ = db.ulic.insert(teryt.parse(datasets.get_teryt_data('ULIC'))) # The central catalogue of streets
    _ = db.simc.insert(teryt.parse(datasets.get_teryt_data('SIMC'))) # System of identifiers and localities names SIMC
    _ = db.wmrodz.insert(teryt.parse(datasets.get_teryt_data('WMRODZ'))) # Auxiliary data for SIMC system


    woj = db.terc.find_one({'NAZWA': 'MAZOWIECKIE'})['WOJ']
    pandas.DataFrame((city for city in db.raw.distinct('address.city') if not db.simc.find_one({"WOJ" : woj, "NAZWA": city})), columns=['city'])




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>city</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0 </th>
      <td>                          None</td>
    </tr>
    <tr>
      <th>1 </th>
      <td>                           139</td>
    </tr>
    <tr>
      <th>2 </th>
      <td>                             2</td>
    </tr>
    <tr>
      <th>3 </th>
      <td>                            32</td>
    </tr>
    <tr>
      <th>4 </th>
      <td>                            34</td>
    </tr>
    <tr>
      <th>5 </th>
      <td>                        Płocka</td>
    </tr>
    <tr>
      <th>6 </th>
      <td>                        Warsaw</td>
    </tr>
    <tr>
      <th>7 </th>
      <td>             Warszawa, Ludwiki</td>
    </tr>
    <tr>
      <th>8 </th>
      <td>      Warszawa@Warszawa Bemowo</td>
    </tr>
    <tr>
      <th>9 </th>
      <td>     Warszawa@Warszawa Bielany</td>
    </tr>
    <tr>
      <th>10</th>
      <td>     Warszawa@Warszawa Mokotów</td>
    </tr>
    <tr>
      <th>11</th>
      <td>      Warszawa@Warszawa Ochota</td>
    </tr>
    <tr>
      <th>12</th>
      <td>       Warszawa@Warszawa Praga</td>
    </tr>
    <tr>
      <th>13</th>
      <td>       Warszawa@Warszawa Ursus</td>
    </tr>
    <tr>
      <th>14</th>
      <td>     Warszawa@Warszawa Ursynów</td>
    </tr>
    <tr>
      <th>15</th>
      <td>     Warszawa@Warszawa Wilanów</td>
    </tr>
    <tr>
      <th>16</th>
      <td>        Warszawa@Warszawa Wola</td>
    </tr>
    <tr>
      <th>17</th>
      <td>      Warszawa@Warszawa Włochy</td>
    </tr>
    <tr>
      <th>18</th>
      <td> Warszawa@Warszawa Śródmieście</td>
    </tr>
    <tr>
      <th>19</th>
      <td>                        warsaw</td>
    </tr>
    <tr>
      <th>20</th>
      <td>                      warszawa</td>
    </tr>
  </tbody>
</table>
<p>21 rows × 1 columns</p>
</div>



Replace entries provided using `city_name@city_name district` format with
`city_name`


    for x in db.raw.find({'address.city': re.compile('\w+@.*')}):
        x['address']['city'] = x['address']['city'].split('@')[0]
        db.raw.save(x)

Titlecase `address.city` field


    for x in db.raw.find({'address.city': re.compile('^[a-z]')}):
        x['address']['city'] = x['address']['city'].title()
        db.raw.save(x)

Replace english name with the local one


    db.raw.update({'address.city': 'Warsaw'}, {'$set': {'address.city': 'Warszawa'}}, multi=True)




    {u'connectionId': 22,
     u'err': None,
     u'n': 44,
     u'ok': 1.0,
     u'updatedExisting': True}



It looks like Ludwiki is actually a street name:


    db.raw.find_one({'address.city': 'Warszawa, Ludwiki'}, {'address': 1, '_id': 0})




    {u'address': {u'city': u'Warszawa, Ludwiki',
      u'housenumber': u'5',
      u'interpolation': u'odd',
      u'postcode': u'01-226',
      u'street': u'Ludwiki'}}




    db.raw.find({'address.city': 'Warszawa', 'address.street': 'Ludwiki'}).count()




    6




    db.raw.update({'address.city': 'Warszawa, Ludwiki'}, {'$set': {'address.city': 'Warszawa'}})




    {u'connectionId': 22,
     u'err': None,
     u'n': 1,
     u'ok': 1.0,
     u'updatedExisting': True}



Remaining entries are a litlle bit harder to deal with.


    list(db.raw.find({'address.city': re.compile('^[0-9]+$')}, {'address': 1, '_id': 0}))




    [{u'address': {u'city': u'139', u'street': u'Radzymi\u0144ska'}},
     {u'address': {u'city': u'2',
       u'housenumber': u'2',
       u'street': u'S\u0142upecka'}},
     {u'address': {u'city': u'32',
       u'housenumber': u'34',
       u'street': u'Gr\xf3jecka'}},
     {u'address': {u'city': u'34',
       u'housenumber': u'34',
       u'street': u'Gr\xf3jecka'}}]




    list(db.raw.find({'address.city': u'Płocka'}, {'address': 1, '_id': 0}))




    [{u'address': {u'city': u'P\u0142ocka', u'housenumber': u'22'}},
     {u'address': {u'city': u'P\u0142ocka', u'housenumber': u'20'}},
     {u'address': {u'city': u'P\u0142ocka', u'housenumber': u'18'}}]



Although it is easy to check manually that all of these are the locations in the
middle of the Warsaw and Płocka is actually a street name for the larger dataset
it would be better to take some kind of semi-automated approach.

One possibility is to use MongoDB geospatial features to identify nodes located
near the node of interest and to use these to predict values of the problematic
fields. Another approach would be to use one of the geocoding services (for
example Google Reverse Geocoding API).

We should keep in mind that both solutions are higly dependent on the quality of
the existing data and can be affected by errors in referrence data as well as
poor coverage of the area of interest.
Depending on our goal usage of the external services can be also limited by some
legal issues.

Let's try both here, but first we have to modify our database.

MongoDB expects that geospatial coordinates are in long-lat format.


    for x in db.raw.find({'pos': {'$exists': True}, 'loc': {'$exists': False}}):
        x['loc'] = x['pos'][::-1]
        db.raw.save(x)

Now we create geospatial index on 'loc' field.


      db.raw.create_index([("loc", pymongo.GEOSPHERE)])




    u'loc_2dsphere'



Let's create some partials which can passed to predict function.


    proximity_func_city = partial(
        cleanup.predict_address_from_coordinates,
        db = db, collection = 'raw', field = 'city', max_distance=100,
    )
    
    google_func_city = partial(
        cleanup.get_address_from_google, field = 'city',
        api_key=GOOGLE_GEOCODING_API_KEY
    )
    
    proximity_func_street = partial(
        cleanup.predict_address_from_coordinates,
        db = db, collection = 'raw', field = 'street', max_distance=10,
    )
    
    google_func_street = partial(
        cleanup.get_address_from_google, field = 'street',
        api_key=GOOGLE_GEOCODING_API_KEY
    )

Let's try our solutions


    rows = []
    
    for doc in db.raw.find({'address.city': re.compile('(^[0-9]+$)|Płocka')}):
        row = [('OSM city', doc['address'].get('city')), ('OSM street', doc['address'].get('street'))]
        
        row += zip(['GEO city', 'GEO city fraction'], cleanup.predict_address(db, 'raw', doc, proximity_func_city)[0])
        row += zip(['GEO street', 'GEO street fraction'], cleanup.predict_address(db, 'raw', doc, proximity_func_street)[0])
        
        if GOOGLE_GEOCODING_API_KEY:
            row += zip(['Google city', 'Google city fraction'], cleanup.predict_address(db, 'raw', doc, google_func_city)[0])
            row += zip(['Google street', 'Google street fraction'], cleanup.predict_address(db, 'raw', doc, google_func_street)[0])
    
        rows.append(dict(row))
    
    pandas.DataFrame(rows)
        




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>GEO city</th>
      <th>GEO city fraction</th>
      <th>GEO street</th>
      <th>GEO street fraction</th>
      <th>Google city</th>
      <th>Google city fraction</th>
      <th>Google street</th>
      <th>Google street fraction</th>
      <th>OSM city</th>
      <th>OSM street</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td> Warszawa</td>
      <td> 1</td>
      <td> Radzymińska</td>
      <td> 1.0</td>
      <td> Warszawa</td>
      <td> 1</td>
      <td> Radzymińska</td>
      <td> 0.6</td>
      <td>    139</td>
      <td> Radzymińska</td>
    </tr>
    <tr>
      <th>1</th>
      <td> Warszawa</td>
      <td> 1</td>
      <td>    Słupecka</td>
      <td> 0.5</td>
      <td> Warszawa</td>
      <td> 1</td>
      <td>    Słupecka</td>
      <td> 0.5</td>
      <td>      2</td>
      <td>    Słupecka</td>
    </tr>
    <tr>
      <th>2</th>
      <td> Warszawa</td>
      <td> 1</td>
      <td>    Grójecka</td>
      <td> 1.0</td>
      <td> Warszawa</td>
      <td> 1</td>
      <td>    Grójecka</td>
      <td> 0.8</td>
      <td>     32</td>
      <td>    Grójecka</td>
    </tr>
    <tr>
      <th>3</th>
      <td> Warszawa</td>
      <td> 1</td>
      <td>    Grójecka</td>
      <td> 1.0</td>
      <td> Warszawa</td>
      <td> 1</td>
      <td>    Grójecka</td>
      <td> 0.6</td>
      <td>     34</td>
      <td>    Grójecka</td>
    </tr>
    <tr>
      <th>4</th>
      <td> Warszawa</td>
      <td> 1</td>
      <td>      Płocka</td>
      <td> 1.0</td>
      <td> Warszawa</td>
      <td> 1</td>
      <td>      Płocka</td>
      <td> 1.0</td>
      <td> Płocka</td>
      <td>        None</td>
    </tr>
    <tr>
      <th>5</th>
      <td> Warszawa</td>
      <td> 1</td>
      <td>      Płocka</td>
      <td> 1.0</td>
      <td> Warszawa</td>
      <td> 1</td>
      <td>      Płocka</td>
      <td> 1.0</td>
      <td> Płocka</td>
      <td>        None</td>
    </tr>
    <tr>
      <th>6</th>
      <td> Warszawa</td>
      <td> 1</td>
      <td>      Płocka</td>
      <td> 1.0</td>
      <td> Warszawa</td>
      <td> 1</td>
      <td>      Płocka</td>
      <td> 1.0</td>
      <td> Płocka</td>
      <td>        None</td>
    </tr>
  </tbody>
</table>
<p>7 rows × 10 columns</p>
</div>



At least on this tiny example our approach seems to work pretty well. Since
number of nodes with missing `address.city` field is substantial filling missing
fields could be really beneficial.


    db.raw.find({'address': {'$exists': True}, 'address.city': {'$exists': False}}).count()




    21764



For now let's fix only those with invalid `address.city` values:


    for doc in db.raw.find({'address.city': re.compile('(^[0-9]+$)|Płocka')}):
        doc['city'] = cleanup.predict_address(db, 'raw', doc, proximity_func_city)[0][0]
        doc['address_city_predicted'] = True
        db.raw.save(doc)

We can expect that working with missing `address.street` fields will be much
harder. From the other hand missing `streets` are rare in our dataset and can be
corrected manually.


    db.raw.find({'address': {'$exists': True}, 'address.street': {'$exists': False}}).count()




    381



### Streets

Let's find disinct street values from our dataset and compare it to the
reference data from TERYT


    osm_streets = set(db.raw.distinct('address.street'))


    streets_ulic = set(
        re.sub('\([0-9]\)', '',  (doc["NAZWA_2"] if doc["NAZWA_2"] else "") +  doc["NAZWA_1"]) # Combine TERYT names into single string
        for doc in db.ulic.find({"WOJ": "14",  "POW": "65"}) # TERYT ULIC for WARSAW
    )

Count entries from OSM which dosen't match those from TERYT


    len(osm_streets.difference(streets_ulic))




    326



Let's have closer look. We can use `fuzzywuzzy.process` to obtain a list of
similar street names from TERYT dataset. While it is good solution if we want to
identify some common mistakes it is certainly not enough to build fully
automated solution.


    rows = []
    
    for street in osm_streets.difference(streets_ulic):
        best_match, score = process.extract(street, streets_ulic, limit=1)[0]
        rows.append({'Street': street, 'Best match': best_match, 'Score': score})
    
    pandas.DataFrame(rows)




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Best match</th>
      <th>Score</th>
      <th>Street</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0 </th>
      <td>                        św. Andrzeja Boboli</td>
      <td>  89</td>
      <td>                          Świętego Andrzeja Boboli</td>
    </tr>
    <tr>
      <th>1 </th>
      <td>                                      Żółta</td>
      <td>  90</td>
      <td>                                  S. Starzyńskiego</td>
    </tr>
    <tr>
      <th>2 </th>
      <td>                                     Kątowa</td>
      <td>  90</td>
      <td>                                      Projektowana</td>
    </tr>
    <tr>
      <th>3 </th>
      <td>                           Bitwy pod Lenino</td>
      <td> 100</td>
      <td>                                  Bitwy Pod Lenino</td>
    </tr>
    <tr>
      <th>4 </th>
      <td>                     Ignacego Paderewskiego</td>
      <td>  95</td>
      <td>                       Ignacego Jana Paderewskiego</td>
    </tr>
    <tr>
      <th>5 </th>
      <td>            Walentego Skorochód-Majewskiego</td>
      <td>  90</td>
      <td>                              korochód-Majewskiego</td>
    </tr>
    <tr>
      <th>6 </th>
      <td>                    rtm. Witolda Pileckiego</td>
      <td>  85</td>
      <td>                     Rotmistrza Witolda Pileckiego</td>
    </tr>
    <tr>
      <th>7 </th>
      <td>                                  Lipkowska</td>
      <td>  79</td>
      <td>                                  Lipkowskiej Wody</td>
    </tr>
    <tr>
      <th>8 </th>
      <td>                         Henryka IV Probusa</td>
      <td>  95</td>
      <td>                                   Henryka Probusa</td>
    </tr>
    <tr>
      <th>9 </th>
      <td>             Dezyderego Adama Chłapowskiego</td>
      <td>  95</td>
      <td>                    Adama Dezyderego Chłapowskiego</td>
    </tr>
    <tr>
      <th>10</th>
      <td>                                   Aktorska</td>
      <td>  67</td>
      <td>            EuroTorebki.pl/MojaTorebka.com/TED-MAR</td>
    </tr>
    <tr>
      <th>11</th>
      <td>                             Czerwona Droga</td>
      <td> 100</td>
      <td>                                    Czerwona droga</td>
    </tr>
    <tr>
      <th>12</th>
      <td>                                 Pabianicka</td>
      <td>  90</td>
      <td>                                             Żabia</td>
    </tr>
    <tr>
      <th>13</th>
      <td>   gen. Michała Tokarzewskiego-Karaszewicza</td>
      <td>  94</td>
      <td>      Generała Michała Tokarzewskiego-Karaszewicza</td>
    </tr>
    <tr>
      <th>14</th>
      <td> ppłk. Mieczysława Sokołowskiego "Grzymały"</td>
      <td>  89</td>
      <td> Podpułkownika Mieczysława "Grzymały" Sokołowsk...</td>
    </tr>
    <tr>
      <th>15</th>
      <td>               Edwarda Józefa Abramowskiego</td>
      <td>  95</td>
      <td>                      Józefa Edwarda Abramowskiego</td>
    </tr>
    <tr>
      <th>16</th>
      <td>                            Francesca Nulla</td>
      <td>  83</td>
      <td>                        Pułkownika Francesca Nullo</td>
    </tr>
    <tr>
      <th>17</th>
      <td>                Williama Heerleina Lindleya</td>
      <td>  89</td>
      <td>                              Williama H. Lindleya</td>
    </tr>
    <tr>
      <th>18</th>
      <td>           Aleja Komisji Edukacji Narodowej</td>
      <td>  95</td>
      <td>          Aleja Komisji Edukacji Narodowej ( KEN )</td>
    </tr>
    <tr>
      <th>19</th>
      <td>                ks. Ignacego Kłopotowskiego</td>
      <td>  91</td>
      <td>                   Księdza Ignacego Kłopotowskiego</td>
    </tr>
    <tr>
      <th>20</th>
      <td>                                      Zgoda</td>
      <td>  80</td>
      <td>                                             Zgody</td>
    </tr>
    <tr>
      <th>21</th>
      <td>                       Józefa Siemieńskiego</td>
      <td>  95</td>
      <td>                         Józefa Jana Siemieńskiego</td>
    </tr>
    <tr>
      <th>22</th>
      <td>                                    Płomyka</td>
      <td>  92</td>
      <td>                                           Promyka</td>
    </tr>
    <tr>
      <th>23</th>
      <td>                                Grocholicka</td>
      <td>  81</td>
      <td>                                 Trakt Grocholicki</td>
    </tr>
    <tr>
      <th>24</th>
      <td>                        Józefa Chłopickiego</td>
      <td>  95</td>
      <td>                      Generała Józefa Chłopickiego</td>
    </tr>
    <tr>
      <th>25</th>
      <td>                             św. Bonifacego</td>
      <td>  86</td>
      <td>                               Świętego Bonifacego</td>
    </tr>
    <tr>
      <th>26</th>
      <td>                                     Łąkowa</td>
      <td>  90</td>
      <td>                                          Bratkowa</td>
    </tr>
    <tr>
      <th>27</th>
      <td>                            Wołodyjowskiego</td>
      <td>  90</td>
      <td>                           Michała Wołodyjowskiego</td>
    </tr>
    <tr>
      <th>28</th>
      <td>                    Wenantego Burdzińskiego</td>
      <td>  87</td>
      <td>                                  W. Burdzińskiego</td>
    </tr>
    <tr>
      <th>29</th>
      <td>                   Samuela Bogumiła Lindego</td>
      <td>  89</td>
      <td>                                Samuela B. Lindego</td>
    </tr>
    <tr>
      <th>30</th>
      <td>               kard. Aleksandra Kakowskiego</td>
      <td>  92</td>
      <td>                  Kardynała Aleksandra Kakowskiego</td>
    </tr>
    <tr>
      <th>31</th>
      <td>                  gen. Sylwestra Kaliskiego</td>
      <td>  91</td>
      <td>                     Generała Sylwestra Kaliskiego</td>
    </tr>
    <tr>
      <th>32</th>
      <td>                               Białobrzeska</td>
      <td> 100</td>
      <td>                                      BIAŁOBRZESKA</td>
    </tr>
    <tr>
      <th>33</th>
      <td>                           Erika Dahlbergha</td>
      <td>  94</td>
      <td>                                  Eryka Dahlbergha</td>
    </tr>
    <tr>
      <th>34</th>
      <td>                             Zielonych Traw</td>
      <td>  72</td>
      <td>                                               AWF</td>
    </tr>
    <tr>
      <th>35</th>
      <td>               Czesława Ludwika Rybińskiego</td>
      <td>  90</td>
      <td>                           Czesława L. Rybińskiego</td>
    </tr>
    <tr>
      <th>36</th>
      <td>                     Janusza Groszkowskiego</td>
      <td>  95</td>
      <td>                  Profesora Janusza Groszkowskiego</td>
    </tr>
    <tr>
      <th>37</th>
      <td>                       Icchoka Lejba Pereca</td>
      <td>  95</td>
      <td>                              Icchaka Lejba Pereca</td>
    </tr>
    <tr>
      <th>38</th>
      <td>                 ks. Zygmunta Trószyńskiego</td>
      <td>  90</td>
      <td>                    Księdza Zygmunta Trószyńskiego</td>
    </tr>
    <tr>
      <th>39</th>
      <td> ppłk. Mieczysława Sokołowskiego "Grzymały"</td>
      <td>  95</td>
      <td>              Mieczysława "Grzymały" Sokołowskiego</td>
    </tr>
    <tr>
      <th>40</th>
      <td>                               Stryjeńskich</td>
      <td>  90</td>
      <td>                         Stryjeńskich 15B lok. 113</td>
    </tr>
    <tr>
      <th>41</th>
      <td>                      Jana Bytnara "Rudego"</td>
      <td>  98</td>
      <td>                            Janka Bytnara "Rudego"</td>
    </tr>
    <tr>
      <th>42</th>
      <td>                   Plac gen. Józefa Hallera</td>
      <td>  90</td>
      <td>                      Plac generała Józefa Hallera</td>
    </tr>
    <tr>
      <th>43</th>
      <td>                        Piotra Maszyńskiego</td>
      <td>  81</td>
      <td>                               Piotra Olszewskiego</td>
    </tr>
    <tr>
      <th>44</th>
      <td>                                       Urle</td>
      <td>  67</td>
      <td>                                   Wacława Kuleszy</td>
    </tr>
    <tr>
      <th>45</th>
      <td>      gen. Felicjana Sławoja Składkowskiego</td>
      <td>  93</td>
      <td>         Generała Felicjana Sławoja Składkowskiego</td>
    </tr>
    <tr>
      <th>46</th>
      <td>             Plac Jana Henryka Dąbrowskiego</td>
      <td>  86</td>
      <td>                Generała Jana Henryka Dąbrowskiego</td>
    </tr>
    <tr>
      <th>47</th>
      <td>             Henryka Melcera-Szczawińskiego</td>
      <td>  90</td>
      <td>                                   Henryka Melcera</td>
    </tr>
    <tr>
      <th>48</th>
      <td>                     Ludwiga van Beethovena</td>
      <td>  95</td>
      <td>                            Ludwika van Beethovena</td>
    </tr>
    <tr>
      <th>49</th>
      <td>                       Franciszka Bartoszka</td>
      <td>  95</td>
      <td>                           Franciszka J. Bartoszka</td>
    </tr>
    <tr>
      <th>50</th>
      <td>                       Batalionu Platerówek</td>
      <td>  97</td>
      <td>                            Batalionu "Platerówek"</td>
    </tr>
    <tr>
      <th>51</th>
      <td>                   Kapelanów Armii Krajowej</td>
      <td>  81</td>
      <td>                                      Kapelanów AK</td>
    </tr>
    <tr>
      <th>52</th>
      <td>                              Świętokrzyska</td>
      <td>  90</td>
      <td>                               Metro Świętokrzyska</td>
    </tr>
    <tr>
      <th>53</th>
      <td>                          Stanisława Wigury</td>
      <td>  90</td>
      <td>        Aleja Francizka Żwirki i Stanisława Wigury</td>
    </tr>
    <tr>
      <th>54</th>
      <td>                   Franciszka Adolfa Achera</td>
      <td>  95</td>
      <td>                                 Franciszka Achera</td>
    </tr>
    <tr>
      <th>55</th>
      <td>                                     Łąkowa</td>
      <td>  90</td>
      <td>                                            Makowa</td>
    </tr>
    <tr>
      <th>56</th>
      <td>                    Strona Hugona Kołłątaja</td>
      <td>  90</td>
      <td>                                  Hugona Kołłątaja</td>
    </tr>
    <tr>
      <th>57</th>
      <td>                                     Górska</td>
      <td>  90</td>
      <td>                                       Jasnogórska</td>
    </tr>
    <tr>
      <th>58</th>
      <td>                      Zgrupowania AK "Żubr"</td>
      <td>  95</td>
      <td>                                  Zgrupowania Żubr</td>
    </tr>
    <tr>
      <th>59</th>
      <td>                       Aleja "Solidarności"</td>
      <td>  97</td>
      <td>                                Aleja Solidarności</td>
    </tr>
    <tr>
      <th></th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
  </tbody>
</table>
<p>326 rows × 3 columns</p>
</div>



Military and church ranks should be replaced by its abbreviations


    ranks_mapping = {
        u"Rotmistrza": u"rtm.",
        u"Podpułkownika": u"ppłk.",
     	u"Pułkownika": u"płk.",
        u"Majora": u"mjr",
        u"Kapitana": u"kpt.",
        u"Generała": u"gen.",
        u"marszałka": u"marsz",
        
        u"Świętej": u"św.",
        u"Świętego": u"św.",
        u"Św": u"św.",
    
        u"Błogosławionego": u"bł.",
        u"Bł.": u"bł.",
        u"Księdza": u"ks.",
        u"Kardynała": u"kard.",
    }
    
    for k, v in ranks_mapping.items():
        for doc in db.raw.find({"address.street": re.compile(k, re.I)}):
            doc["address"]["street"] = re.sub(k, v, doc["address"]["street"], flags=re.I)
            db.raw.save(doc)

Now we can correct some common mistakes


    street_names_corrections = {
        # Names of the units of the Armia Krajowa (https://en.wikipedia.org/wiki/Armia_Krajowa)
    	u'Zgrupowania AK "Żmija"': u'Zgrupowania Żmija',
    	u'Kompanii AK "Kordian"': u'Kompanii "Kordian"',
    	u'Plutonu AK "Torpedy"': u'Plutonu Torpedy',
    	u'Batalionu Platerówek': u'Batalionu "Platerówek"',
    	u'Batalionu AK "Pięść"': u'Batalionu "Pięść"',
    	u'Batalionu AK "Zośka"': u'Batalionu Zośka',
    	u'Zgrupowania AK "Żubr"': u'Zgrupowania Żubr',
    	u'Pułku AK "Baszta"': u'Pułku Baszta',
    	u'Batalionu AK "Oaza"': u'Batalionu Oaza',
        
        # Some simple mistakes
    	u'Związku Walki Młodych ( ZWM )': u'ZWM',
    	u'Nadwiślańska': u'NAdwiślańska',
    	u'Czerwona Droga': u'Czerwona droga',
    	u'Bitwy pod Lenino': u'Bitwy Pod Lenino',
    	u'Powstańców Śląskich': u'Powstanców Śląskich',
    	u'Jamesa Gordona Bennetta': u'ordona Bennetta',
    	u'korochód-Majewskiego': u'Walentego Skorochód-Majewskiego',
        u'Białobrzeska': u'BIAŁOBRZESKA',
        
        # Fictional characters
    	u'Wołodyjowskiego': u'Michała Wołodyjowskiego',
    	u'Zagłoby': u'Jana Onufrego Zagłoby',
    	u'Podbipięty': u'Longinusa Podbipięty',
        
        # Foreign names
    	u'Honoriusza Balzaka': u'Honoriusza Balzaca',
    	u'Erika Dahlbergha': u'Eryka Dahlbergha',
    	u'Icchoka Lejba Pereca': u'Icchaka Lejba Pereca',
    	u'Rabindranatha Tagore': u'Rabindrannatha Tagore',
    	u'Ludwiga van Beethovena': u'Ludwika van Beethovena',
    	u'Michaela Faradaya': u'Michała Faradaya',
    	u'Williama Szekspira': u'Wiliama Szekspira',
    	u'Józsefa Antalla': u'Józefa Antalla',
    	u'Moliera': u'Jana Baptysty Moliera',
    	u'Giuseppe Garibaldiego': u'Giuseppe Garbialdiego',
    	u'Isaaca Newtona': u'Izaaka Newtona',
    	u'Béli Bartóka': u'Beli Bartoka',
    	u'Vincenta van Gogha': u'Vincenta Van Gogha',
    	u'Sándora Petöfiego': u'Sandora Petöfiego',
    	u'Canaletta': u'Bernarda Belotta Canaletta',
    	u'Simóna Bolívara': u'Simona Bolivara',
        
        # Polish names
    	u'Jana Bytnara "Rudego"': u'Janka Bytnara "Rudego"',
    	u'Janusza Groszkowskiego': u'Profesora Janusza Groszkowskiego',
    	u'ks. Jana Sitnika': u'ks. prałata Jana Sitnika',
    	u'Aleksandra Prystora': u'Prystora',
    	u'gen. Wiktora Thomméego': u'Wiktora Thomméego',
    	u'Kazimierza Wyki': u'K. Wyki',
    	u'Mariana Sengera "Cichego"': u'Jana Sengera "Cichego"',
    	u'Ludwika Nabielaka': u'Nabielaka',
    	u'płk. Zoltána Baló': u'Zoltána Baló',
    	u'Antoniego Edwarda Odyńca': u'Antoniego E. Odyńca',
    	u'Kazimierza Wóycickiego': u'Wóycickiego',
    	u'Żwirki i Wigury': u'Aleja Francizka Żwirki i Stanisława Wigury',
    	u'Henryka Melcera-Szczawińskiego': u'Henryka Melcera',
    	u'ppłk. Mieczysława Sokołowskiego "Grzymały"': u'Mieczysława "Grzymały" Sokołowskiego',
    	u'ppłk. Mieczysława Sokołowskiego "Grzymały"': u'ppłk. Mieczysława "Grzymały" Sokołowskiego',
    	u'gen. Augusta Emila Fieldorfa "Nila"': u'gen. Emilia Augusta Fieldorfa',
    	u'Bronisława Pietraszewicza "Lota"': u'Bronisława "Lota" Pietraszewicza',
    	u'Józefa Nusbauma-Hilarowicza': u'Józefa Nusbauma',
    }


    for k, v in street_names_corrections.items():
        db.raw.update({"address.street": v}, {"$set": {"address.street": k}}, multi=True)

In a single case there is more than one street in a street field.


    db.raw.find_one({"address.street": re.compile(';')})





    {u'_id': ObjectId('53464e700325b1068ed089fd'),
     u'address': {u'city': u'Warszawa',
      u'housenumber': u'8;20',
      u'street': u'Zab\u0142.cka;Dorodna'},
     u'created': {u'changeset': u'20200679',
      u'timestamp': u'2014-01-25T21:12:18Z',
      u'uid': u'1902220',
      u'user': u'Kubaa',
      u'version': u'4'},
     u'id': u'2025679540',
     u'loc': [20.9847639, 52.303452],
     u'pos': [52.303452, 20.9847639],
     u'type': u'node'}



Since it's not clear which value should be use let's ask Google


    cleanup.predict_address(db, 'raw', db.raw.find_one({"address.street": re.compile(';')}), google_func_street)




    [(u'Dorodna', 1.0)]



Since this value looks good so we can update document accordingly.


    db.raw.update({'address.street': u'Zab\u0142ocka;Dorodna'}, {'$set': {'address.street': u'Dorodna', 'address_street_predicted': True}})




    {u'connectionId': 22,
     u'err': None,
     u'n': 0,
     u'ok': 1.0,
     u'updatedExisting': False}



Finally some entires contain housenumber and flatnumber. We can extract
housenumbers and correct street names.


    pattern = re.compile('([\w\.\s]+)\s+([1-9][0-9]*[a-zA-Z]?)\s*(?:(?:lok.)|/)\s*([1-9][0-9]*)', re.U)


    for s in db.raw.find({"address.street": pattern}).distinct("address.street"):
        print s

    Franciszka Klimczaka 17/80
    Puszczyka 18 lok. 45
    Stryjeńskich 15B lok. 113
    bł. Ładysława z Gielniowa 9/11



    for doc in db.raw.find({"address.street": pattern}):
        street, housenumber, flat = pattern.search(doc['address']['street']).groups()
        doc['address']['housenumber'] = housenumber
        doc['address']['street'] = street
        db.raw.save(doc)
